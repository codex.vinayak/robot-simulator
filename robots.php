<?php

class ToyRobotSimulator {
    private $x;
    private $y;
    private $facing;

    private const DIRECTIONS = ['NORTH', 'EAST', 'SOUTH', 'WEST'];

    public function __construct() {
        $this->x = null;
        $this->y = null;
        $this->facing = null;
    }

    public function executeCommand($command) {
        $parts = explode(' ', $command);
        $action = strtoupper($parts[0]);

        switch ($action) {
            case 'PLACE':
                if (count($parts) !== 2) {
                    echo "Invalid PLACE command format." . PHP_EOL;
                    return;
                }
                $params = explode(',', $parts[1]);
                if (count($params) !== 3) {
                    echo "Invalid PLACE command parameters." . PHP_EOL;
                    return;
                }
                $x = intval($params[0]);
                $y = intval($params[1]);
                $facing = $params[2];
                if ($this->isValidPosition($x, $y, $facing)) {
                    $this->x = $x;
                    $this->y = $y;
                    $this->facing = $facing;
                } else {
                    echo "Invalid position or direction." . PHP_EOL;
                }
                break;
            case 'MOVE':
                if ($this->isPlaced()) {
                    $newX = $this->x;
                    $newY = $this->y;
                    switch ($this->facing) {
                        case 'NORTH':
                            $newY++;
                            break;
                        case 'EAST':
                            $newX++;
                            break;
                        case 'SOUTH':
                            $newY--;
                            break;
                        case 'WEST':
                            $newX--;
                            break;
                    }
                    if ($this->isValidPosition($newX, $newY, $this->facing)) {
                        $this->x = $newX;
                        $this->y = $newY;
                    } else {
                        echo "Invalid move. Robot would fall off the table." . PHP_EOL;
                    }
                }
                break;
            case 'LEFT':
                if ($this->isPlaced()) {
                    $currentDirIndex = array_search($this->facing, self::DIRECTIONS);
                    $newDirIndex = ($currentDirIndex - 1 + 4) % 4;
                    $this->facing = self::DIRECTIONS[$newDirIndex];
                }
                break;
            case 'RIGHT':
                if ($this->isPlaced()) {
                    $currentDirIndex = array_search($this->facing, self::DIRECTIONS);
                    $newDirIndex = ($currentDirIndex + 1) % 4;
                    $this->facing = self::DIRECTIONS[$newDirIndex];
                }
                break;
            case 'REPORT':
                if ($this->isPlaced()) {
                    echo $this->x . ',' . $this->y . ',' . $this->facing . PHP_EOL;
                } else {
                    echo "Robot has not been placed on the table yet." . PHP_EOL;
                }
                break;
            default:
                echo "Invalid command." . PHP_EOL;
        }
    }

    private function isValidPosition($x, $y, $facing) {
        return $x >= 0 && $x <= 4 && $y >= 0 && $y <= 4 && in_array($facing, self::DIRECTIONS);
    }

    private function isPlaced() {
        return $this->x !== null && $this->y !== null && $this->facing !== null;
    }
}

// Main code
$robot = new ToyRobotSimulator();

echo "Toy Robot Simulator" . PHP_EOL;
echo "Enter commands ('EXIT' to quit):" . PHP_EOL;

while (true) {
    $input = readline("> ");
    if (strtoupper($input) === 'EXIT') {
        break;
    }
    $robot->executeCommand($input);
}

echo "Goodbye!" . PHP_EOL;
