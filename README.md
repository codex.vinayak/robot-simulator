# Toy Robot Simulator

Welcome to the Toy Robot Simulator project! This is a simple PHP-based application that simulates the movement of a toy robot on a 5x5 grid tabletop.

## Table of Contents

- [Description](#description)
- [Commands](#commands)
- [Usage](#usage)

## Description

The Toy Robot Simulator application allows a toy robot to move on a 5x5 grid tabletop. The robot responds to a set of commands that control its placement, movement, rotation, and reporting of its current state.

## Commands

The following commands can be used with the simulator:

- `PLACE X,Y,F`: Place the robot on the grid at position `(X, Y)` facing direction `F` (NORTH, EAST, SOUTH, or WEST).
- `MOVE`: Move the robot one unit forward in the direction it's facing.
- `LEFT`: Rotate the robot 90 degrees to the left without changing its position.
- `RIGHT`: Rotate the robot 90 degrees to the right without changing its position.
- `REPORT`: Display the current position and direction of the robot.

## Usage

To run the Toy Robot Simulator:

1. Make sure you have PHP installed on your system.
2. Clone this repository: `git clone https://gitlab.com/codex.vinayak/robot-simulator.git`
3. Navigate to the project directory: `cd robot-simulator`
4. Run the simulator: `php robots.php`

